<?php

class Product extends \Eloquent {
	protected $fillable = ['name', 'description', 'inventory', 'price'];

    protected $hidden = ['created_at', 'updated_at'];
}