<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;
// use Faker\Factory\Provider\Lorem as Lorem;

class ProductsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 30) as $index)
		{
			Product::create([
                'name' => $faker->firstName(),
                // 'description' => 'This is the description of this product',
                'description' => $faker->text($maxNbChars = 200),

                'inventory' => $faker->randomNumber(1),
                'price' => $faker->randomNumber(2),
			]);
		}
	}

}