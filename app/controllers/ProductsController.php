<?php

class ProductsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /product
	 *
	 * @return Response
	 */
	public function index()
	{
		return Response::json(Product::get());
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /product/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /product
	 *
	 * @return Response
	 */
	public function store()
	{
        $product = Product::create([
            'name' => Input::get('name'),
            'price' => Input::get('price'),
            'description' => Input::get('description'),
            'inventory' => Input::get('inventory'),
        ]);

        return $product;
	}

	/**
	 * Display the specified resource.
	 * GET /product/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Response::json(Product::find($id));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /product/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$product = Product::find($id);

        $product->name = Input::get('name');
        $product->price = Input::get('price');
        $product->inventory = Input::get('inventory');
        $product->description = Input::get('description');

        $product->save();

        return $product;
	}


	/**
	 * Remove the specified resource from storage.
	 * DELETE /product/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$product = Product::find($id);
		$product->delete();
		die('DELETED');
	}

}