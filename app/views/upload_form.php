<html>
<head>
	<title>Upload Photo</title>
</head>
<style type="text/css" href="/assets/css/bootstrap.min.css"></style>
<style type="text/css" href="/assets/css/bootstrap-theme.min.css"></style>
<body>

<div class="container">

<?php if (Session::has('users')): ?>
	<h3><?php echo Session::get('message'); ?></h3>
<?php endif; ?>

<?php echo Form::open(array('url' => 'upload', 'method' => 'post', 'files' => true)); ?>
	<?php echo Form::file('filename'); ?>
	<?php echo $errors->first('filename'); ?>
	<?php echo Form::textarea('caption'); ?>
	<?php echo $errors->first('caption'); ?>
	<?php echo Form::select('breed_id', array('' => 'Please select breed') + $breeds, ''); ?>
	<?php echo $errors->first('breed_id'); ?>
	<?php echo Form::submit('Upload'); ?>
<?php echo Form::close(); ?>

</div>

<script type="text/javascript" src="/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="/assets/js/main.js"></script>
<script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
</body>
</html>