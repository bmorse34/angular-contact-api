<html>
<head>
	<title>Upload Photo</title>
</head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/bootstrap-theme.min.css" rel="stylesheet">
<body>

<div class="container">

	<?php foreach($photos as $photo): ?>

		<div class="col-sm-4" style="border:1px solid red;">
			<p><?php echo $photo->caption; ?></p>
			<img src="/resized_images/<?php echo $photo->filename; ?>" width="300px" class="img-responsive">
			<p><?php echo $photo->breed->name; ?></p>
		</div>
	<?php endforeach; ?>
</div>

<script type="text/javascript" src="/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="/assets/js/main.js"></script>
<script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
</body>
</html>