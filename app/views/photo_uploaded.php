<html>
<head>
	<title>Thanks!</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1"/>
  <link type="text/css" media="screen" rel="stylesheet" href="/assets/css/cropper.min.css">

</head>
<body>

<img class="cropper" alt="" src="/user_images/barge-canal-sunset.jpg" cropwidth="500" cropheight="500"/>


<button class="btn btn-primary" id="getDataURL" type="button">Get Data URL</button>

 <div class="download">
    <a href="#" download="crop.png">Download</a>
  </div>

          <textarea class="form-control" id="dataURL" rows="10"></textarea>

  <a href="#" class="get-coordinates">Get coordinates</a>

<script type="text/javascript" src="/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="/assets/js/cropper.min.js"></script>
<script type="text/javascript" src="/assets/js/main.js"></script>
</body>
</html>