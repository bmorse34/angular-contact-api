<html>
<head>
	<title>All Photos</title>
</head>
<body>

<h1>All photos</h1>

<?php print_r($breed_photos); ?>

<?php foreach($photos as $photo): ?>
	<p><?php echo $photo->caption; ?></p>
	<p><img src="/user_images/<?php echo $photo->filename; ?>"></p>
	<p><?php echo $photo->breed->name; ?></p>

<?php endforeach; ?>

<script type="text/javascript" src="/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="/assets/js/main.js"></script>
</body>
</html>