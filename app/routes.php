<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::get('api/contacts', 'ContactsController@index');
Route::post('api/contacts', 'ContactsController@store');
Route::get('api/contacts/{id}', 'ContactsController@show');
Route::put('api/contacts/{id}', 'ContactsController@update');
Route::delete('api/contacts/{id}', 'ContactsController@destroy');

Route::get('api/products', 'ProductsController@index');
Route::post('api/products', 'ProductsController@store');
Route::get('api/products/{id}', 'ProductsController@show');
Route::put('api/products/{id}', 'ProductsController@edit');
Route::delete('api/products/{id}', 'ProductsController@destroy');